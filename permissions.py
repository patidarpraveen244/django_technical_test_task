from rest_framework import permissions
from django.contrib.auth.models import User


class HasUpdatePermission(permissions.BasePermission):
    
    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user == obj.owner:
            return True
        return False