from django.shortcuts import render
from permissions import HasUpdatePermission

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from pets.models import Cat, Dog
from pets.serializers import CatSerializer, DogSerializer


class CatList(generics.ListCreateAPIView):
    """
    API to get Cat List and Create a new object
    """
    queryset = Cat.objects.all()
    serializer_class = CatSerializer

cat_list = CatList.as_view()


class CatDetail(generics.RetrieveUpdateDestroyAPIView):
    """
        API Detail View for Read, update, and Delete an object
    """
    queryset = Cat.objects.all()
    serializer_class = CatSerializer
    permission_classes = (IsAuthenticated, HasUpdatePermission)
    lookup_url_kwarg = 'cat_id'

cat_detail = CatDetail.as_view()

class DogList(generics.ListCreateAPIView):
    """
        API to get Cat List and Create a new object
    """
    queryset = Dog.objects.all()
    serializer_class = DogSerializer

dog_list = DogList.as_view()


class DogDetail(generics.RetrieveUpdateDestroyAPIView):
    """
        API Detail View for Read, update, and Delete an object
    """
    queryset = Dog.objects.all()
    serializer_class = DogSerializer
    permission_classes = (IsAuthenticated, HasUpdatePermission)
    lookup_url_kwarg = 'dog_id'

dog_detail = DogDetail.as_view()