from rest_framework import serializers
from pets.models import Cat, Dog

class CatSerializer(serializers.ModelSerializer):
    """
    Model Serializer for Cat
    """

    class Meta:
        fields = '__all__'
        model = Cat

class DogSerializer(serializers.ModelSerializer):
    """
    Model Serializer for Dog
    """
    
    class Meta:
        fields = '__all__'
        model = Dog
