# Generated by Django 2.2.6 on 2019-10-29 08:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pets', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cats',
            options={'verbose_name': 'Cat', 'verbose_name_plural': 'Cats'},
        ),
        migrations.AlterModelOptions(
            name='dogs',
            options={'verbose_name': 'Dog', 'verbose_name_plural': 'Dogs'},
        ),
    ]
