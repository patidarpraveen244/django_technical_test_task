from django.urls import path
from pets import views

urlpatterns = [
    path('cats/', views.cat_list),
    path('cats/<int:cat_id>/', views.cat_detail),
    path('dogs/', views.dog_list),
    path('dogs/<int:dog_id>/', views.dog_detail),
]
