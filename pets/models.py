from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Animals(models.Model):
    """
    Parent Class for Animals that can be inherited by animals to use the common properties.
    """
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.TextField(max_length=50)
    birthday = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Cat(Animals):
    """
    Child Class of Animals representing Cat that inherits all the properties of Animals
    """
    
    class Meta:
        verbose_name = 'Cat'
        verbose_name_plural = 'Cats'


class Dog(Animals):
    """
    Child Class of Animals representing Dog that inherits all the properties of Animals
    """

    class Meta:
        verbose_name = 'Dog'
        verbose_name_plural = 'Dogs'
