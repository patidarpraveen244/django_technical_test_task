Site operation instructions :

1. Open the URL to GET Cat Objects (https://drf-test-task.herokuapp.com/api/cats/)
2. Login using TEST USER CREDENTIALS as given below if using the ADMIN view (https://drf-test-task.herokuapp.com/admin/)
3. Perform CRUD operations
4. Perform similarly for Dog objects as well
5. User auth has not been included separately and its handling has been delegated to Django's in-build admin panel
6. Please feel free to test the logic of only the actual owner being able to update Cats and Dogs by creating additional Owner with appropriate permissions using the admin panel. Admin credentials can be found below

Admin Heroku URL: (https://drf-test-task.herokuapp.com/admin/)

GET Cat Objects:  (https://drf-test-task.herokuapp.com/api/cats/)

Specific Cat Objects: (https://drf-test-task.herokuapp.com/api/cats/<cat_id>/)

GET Dog Objects:      (https://drf-test-task.herokuapp.com/api/dogs)

Specific Dog Objects:   (https://drf-test-task.herokuapp.com/api/dogs/<dog_id>/)

**TEST USER CREDENTIALS:**

`username: test_user`

`password: test@123`


**ADMIN CREDENTIALS**:

`admin_user / testproject.123`